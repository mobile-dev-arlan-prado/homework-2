package edu.sjsu.android.zoodirectory;

import java.util.ArrayList;
import java.util.List;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;


public class MyAdapter extends RecyclerView.Adapter<MyAdapter.ViewHolder> {
    private List<Animal> values;
    private Context context;

    public class ViewHolder extends RecyclerView.ViewHolder{
        public TextView txtTitle;
        public ImageView image;
        public View layout;

        public ViewHolder(View v){
            super(v);
            context = v.getContext();
            layout = v;
            txtTitle = (TextView) v.findViewById(R.id.animalName);
            image = (ImageView) v.findViewById(R.id.animalIcon);
        }

    }

    public void add(int position, Animal item){
        values.add(position, item);
        notifyItemInserted(position);
    }

    public MyAdapter(List<Animal> myDataset){
        values = myDataset;
    }


    @Override
    public MyAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType){
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View v = inflater.inflate(R.layout.main_custom_row, parent, false);
        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position){
        final Animal temp = values.get(position);
        holder.image.setImageDrawable(context.getResources().getDrawable(temp.getImageLink()));
        holder.txtTitle.setText(temp.getName());
        holder.txtTitle.setOnClickListener(new OnClickListener(){
            @Override
            public void onClick(View v){
                //remove(position);
                //put view onto the animal view
                Intent myIntent = new Intent(context, AnimalActivity.class);
                myIntent.putExtra("aname", temp.getName());
                myIntent.putExtra("adesc", temp.getDesc());
                myIntent.putExtra("aimag", temp.getImageLink());
                context.startActivity(myIntent);

            }
        });

    }
    @Override
    public int getItemCount(){
        return values.size();
    }
}
