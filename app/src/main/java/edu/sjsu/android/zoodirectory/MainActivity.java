package edu.sjsu.android.zoodirectory;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.ClipData;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

import java.util.ArrayList;
import java.util.List;
class Animal{
    private String name;
    private String description;
    private int imageLink;

    public Animal(String name,String description,int imageLink){
        this.name = name;
        this.description = description;
        this.imageLink = imageLink;
    }
    public void setAnimal(String name,String description,int imageLink){
        this.name = name;
        this.description = description;
        this.imageLink = imageLink;
    }
    public String getName(){
        return name;
    }

    public String getDesc() {
        return description;
    }

    public int getImageLink(){
        return imageLink;
    }
}
public class MainActivity extends AppCompatActivity {
    private RecyclerView recyclerView;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager layoutManager;
    private List<Animal> animalList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        recyclerView = (RecyclerView) findViewById(R.id.my_recycler_view);
        recyclerView.setHasFixedSize(true);
        layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);
        initList();
        //populate ArrayList

        mAdapter = new MyAdapter(animalList);
        recyclerView.setAdapter(mAdapter);



    }

    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
            case R.id.information:
                Intent informationIntent = new Intent(this, InformationActivity.class);
                startActivity(informationIntent);
                return true;
            case R.id.uninstall:
                Intent uninstallIntent = new Intent(Intent.ACTION_DELETE, Uri.parse("package:" + getPackageName()));
                startActivity(uninstallIntent);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    protected void initList(){
        //Drawable drawable = getResources().getDrawable(R.drawable.dog);
        animalList = new ArrayList<>();
        String temp = "The aardvark is a medium-sized, burrowing, nocturnal mammal native to Africa." +
                " It is the only living species of the order Tubulidentata, although other prehistoric species and genera of Tubulidentata are known. Unlike other insectivores, it has a long pig-like snout, which is used to sniff out food. It roams over most of the southern two-thirds of the African continent, avoiding areas that are mainly rocky. A nocturnal feeder, it subsists on ants and termites, which it will dig out of their hills using its sharp claws and powerful legs. It also digs to create burrows in which to live and rear its young. It receives a \"least concern\" rating from the IUCN, although its numbers seem to be decreasing.";
        animalList.add(new Animal("Aardvark", temp, R.drawable.aardvark));

        temp = "The capybara (Hydrochoerus hydrochaeris) is a giant cavy rodent native to South America. It is the largest living rodent in the world." +
                "Its close relatives include guinea pigs and rock cavies, and it is more distantly related to the agouti, the chinchilla, and the coypu. The capybara inhabits savannas and dense forests and lives near bodies of water. It is a highly social species and can be found in groups as large as 100 individuals, but usually lives in groups of 10–20 individuals. The capybara is not a threatened species, but it is hunted for its meat and hide and also for grease from its thick fatty skin.";
        animalList.add(new Animal("Capybara", temp, R.drawable.capybara));

        temp = "Zebras are African equines with distinctive black-and-white striped coats. Zebra stripes come in different patterns, unique to each individual. Several theories have been proposed for the function of these stripes, with most evidence supporting them as a form of protection from biting flies. Zebras inhabit eastern and southern Africa and can be found in a variety of habitats such as savannahs, grasslands, woodlands, shrublands and mountainous areas. ";
        animalList.add(new Animal("Zebra", temp, R.drawable.zebra));
    }
}