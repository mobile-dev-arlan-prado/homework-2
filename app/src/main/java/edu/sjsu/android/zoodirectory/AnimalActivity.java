package edu.sjsu.android.zoodirectory;

import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;


public class AnimalActivity extends AppCompatActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.animal_page);
        Bundle myInput = this.getIntent().getExtras();
        TextView t = new TextView(this);
        t = (TextView) findViewById(R.id.animalNameView);
        t.setText(myInput.getString("aname"));
        t = (TextView) findViewById(R.id.animalDescriptionView);
        t.setText(myInput.getString("adesc"));
        ImageView i = new ImageView(this);
        i = (ImageView) findViewById(R.id.animalImageView);
        i.setImageDrawable(getResources().getDrawable(myInput.getInt("aimag")));

    }

    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
            case R.id.information:
                Intent informationIntent = new Intent(this, InformationActivity.class);
                startActivity(informationIntent);
                return true;
            case R.id.uninstall:
                Intent uninstallIntent = new Intent(Intent.ACTION_DELETE, Uri.parse("package:" + getPackageName()));
                startActivity(uninstallIntent);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
